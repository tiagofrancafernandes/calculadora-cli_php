# Calculadora em PHP para usar no terminal.

### Acesse a pasta onde ficará a calculadora
	cd /pasta/onde/ficara/a/calculadora
	
### Clone o repositório
	git clone https://tiagofrancafernandes@bitbucket.org/tiagofrancafernandes/calculadora-cli_php.git
	
### Crie um alias para a calculadora	
	alias calc='php /pasta/onde/ficara/a/calculadora/calculadora-cli_php/smat'
	export alias calc='php /pasta/onde/ficara/a/calculadora/calculadora-cli_php/smat'
	
* Obs.: Não use 'php -f', mas se for usar, ao final do alias coloque '--' para que funcione com números/parâmetros negativos. Ex: 
```shell
alias calc='php -f /smat --'
```

#### Dica:
	Coloque o alias no arquivo "~/.bashrc"
	Em seguida carregue-o digitando:


```shell
~$source ~/.bashrc
```	
	
	
	
## Como usar a calculadora
	
```shell
	-------------------------------------------
	| Ex:
	| calc 2 + 2
	| calc 2 x 2
	-------------------------------------------
	

	------------------------------------------
	| Operadores Aritméticos Disponíveis:
	| Soma = "+"
	| Subtração = "-"
	| Multiplicação = "x" ou "X"
	| Divisão = "/"
	------------------------------------------

```

### TODO / A fazer

- [ ] Expressões numéricas
- [ ] Resto da operação
- [ ] Diversas entradas ex: '2 + 5 - 3' (Por enquanto só é permitido 2)