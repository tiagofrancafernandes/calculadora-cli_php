<?php
// var_dump($argv);
function help()
{
	$msg = "
	-------------------------------------------
	| Ex:
	| calc 2 + 2
	| calc 2 x 2
	-------------------------------------------
	

	------------------------------------------
	| Operadores Aritméticos Disponíveis:
	| Soma = \"+\"
	| Subtração = \"-\"
	| Multiplicação = \"x\" ou \"X\"
	| Divisão = \"/\"
	------------------------------------------
	";
	echo $msg;
}

function not_number()
{
	$msg = "Erro: Não numérico.\n";
	echo $msg;
}


if (!isset($argv[1])) {
	echo "Nenhum parametro foi passado.\n\n";
	echo "Para ajuda digite \"-h\" ou \"--help\"\n\n";
	exit;
}else{
	if($argv[1] == "h" || $argv[1] == "help" || $argv[1] == "ajuda"){
		help();
		exit();
	}
}

if(isset($argv[1]) AND isset($argv[2]) AND isset($argv[3])){
	if(!is_numeric($argv[1]) || !is_numeric($argv[3])){
		not_number();
		exit();
	}

	//Checa se é um desses operadores aritiméticos ( + - * / )
	//Se não for, sai
	switch ($argv[2]) {
		case '+':
		case '-':
		case 'x':
		case 'X':
		case '/':
			continue;
			break;
		
		default:
			exit;
			break;
	}
	
	switch ($argv[2]) {
		case '+':
		echo "-----------------\n| $argv[1] + $argv[3]";
		echo "\n-----------------\n| = ";
		echo $argv[1] + $argv[3];
		echo "\n-----------------";
		echo "\n";
		exit();
			break;
		
		case '-':
		echo "-----------------\n| $argv[1] - $argv[3]";
		echo "\n-----------------\n| = ";
		echo $argv[1] - $argv[3];
		echo "\n-----------------";
		echo "\n";
		exit();
			break;
		
		case 'x':
		case 'X':
		echo "-----------------\n| $argv[1] * $argv[3]";
		echo "\n-----------------\n| = ";
		echo $argv[1] * $argv[3];
		echo "\n-----------------";
		echo "\n";
		exit();
			break;
		
		case '/':
		echo "-----------------\n| $argv[1] / $argv[3]";
		echo "\n-----------------\n| = ";
		echo $argv[1] / $argv[3];
		echo "\n-----------------";
		echo "\n";
		exit();
			break;
		
		default:
			help();
			break;
	}

}else{
	
echo "	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	|Os parâmetros estão incorretos.
	--------------------------------";
	help();
}


?>
